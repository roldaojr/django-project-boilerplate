FROM python:3-slim as base
EXPOSE 8000
ENV PYTHONUNBUFFERED=1
ENV VIRTUAL_PORT=8000
ENV BUILD_DEPS="git python3-dev libpq-dev build-essential"
RUN mkdir /app
WORKDIR /app
ADD Pipfile.lock /app/
ADD Pipfile /app/
RUN apt-get update && apt install -y --no-install-recommends $BUILD_DEPS \
    && pip3 install --no-cache-dir pipenv \
    && pipenv install --system --deploy --clear \
    && apt-get remove --purge -y $BUILD_DEPS \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

FROM base as app
COPY . /app/
RUN python manage.py collectstatic --no-input
CMD gunicorn -c gunicorn.cfg project_name.wsgi
