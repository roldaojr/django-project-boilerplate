import os
import environ
import importlib

# env defaults
env_settings = {
    'DEBUG': (bool, True),
    'SECRET_KEY': (str, 'dummy'),
    'ALLOWED_HOSTS': (list, ['*']),
}

# read .env file
root = environ.Path(__file__) - 2
env = environ.Env(**env_settings)
if os.path.isfile(root('.env')):
    env.read_env(root('.env'))

PROJECT_PACKAGE_NAME = os.path.basename(environ.Path(__file__) - 1)
BASE_DIR = root()
SECRET_KEY = env('SECRET_KEY')
DEBUG = env('DEBUG')
ALLOWED_HOSTS = env('ALLOWED_HOSTS')
INTERNAL_IPS = ['127.0.0.1']
USE_X_FORWARDED_HOST = True
ROOT_URLCONF = 'project_name.urls'
WSGI_APPLICATION = 'project_name.wsgi.application'

INSTALLED_APPS = [
    'project_name.core',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'whitenoise.runserver_nostatic'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': env.db(default='sqlite:///db.sqlite3')
}
CACHES = {
    'default': env.cache(default='locmemcache://'),
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# i18n/l10n config
LANGUAGE_CODE = env('LANGUAGE_CODE', default='pt-br')
TIME_ZONE = env('TIME_ZONE', default='America/Sao_Paulo')
USE_I18N = True
USE_L10N = True
USE_TZ = True
# static files config
STATIC_URL = env('STATIC_URL', default='/static/')
STATIC_ROOT = env('STATIC_ROOT', default=root('static'))
MEDIA_URL = env('MEDIA_URL', default='/media/')
MEDIA_ROOT = env('MEDIA_ROOT', default=root('media'))
# Email config
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default=None)
vars().update(env.email_url('EMAIL_URL', default='consolemail://'))

if not DEBUG:
    STATICFILES_STORAGE = 'whitenoise.storage.CompressedStaticFilesStorage'

# Configure debug toolbar
DEBUG_TOOLBAR = env('DEBUG_TOOLBAR', default=DEBUG)
dt_spec = importlib.util.find_spec('debug_toolbar')
if dt_spec is None:
    DEBUG_TOOLBAR = False

if DEBUG_TOOLBAR:
    INSTALLED_APPS += ['debug_toolbar']
    MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']
    DEBUG_TOOLBAR_CONFIG = {
        'SHOW_COLLAPSED': True
    }
