# django project boilerplate

## Instruções Iniciais

Baixar / Clonar este repositório.

Instalar o Pip e PipEnv

    sudo apt-get install python3-pip python-dev build-essential
    sudo pip3 install --upgrade pip
    sudo pip3 install pipenv

Instalar as dependencias. Entre na pasta do projeto e execute o seguinte comando:

    pipenv install --dev

Renomear o projeto

    pipenv run manage rename project_name <nome-do-novo-projeto>

Copiar arquivo **.env.template** para **.env** e fazer as alterações necessárias.

Criar estrutura banco de dados

    pipenv run manage migrate

Criar usuário inicial (superusuário)

    pipenv run manage createsuperuser

Executando o projeto

    pipenv run server

## Implantação usando docker

Copiar arquivo **.env.template** para **.env** e fazer as alterações necessárias.

Construir o container

    docker compose build

Executar

    docker compose up -d

Criar/atualizar banco de dados

    docker compose exec django python mange.py migrate

Criar usuário inicial (superusuário)

    docker compose exec django python mange.py createsuperuser
